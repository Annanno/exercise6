package com.example.exercise6

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.ActivityResultRegistry
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import com.example.exercise6.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        init()
    }

    private fun init() {
        binding.changeBtn.setOnClickListener {
            openSecondActivity()
        }

    }

    private fun openSecondActivity() {
        val intent = Intent(applicationContext, ChangeInfoActivity::class.java)
        resultLauncher.launch(intent)
    }


    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val info: UserInfo? = data?.getParcelableExtra("result")
                if (info != null) {
                    binding.firsName.text = info.name
                    binding.lastName.text = info.lastName
                    binding.email.text = info.email
                    binding.birthYear.text = info.birthYear
                    binding.gender.text = info.gender
                }


            }
        }



}


