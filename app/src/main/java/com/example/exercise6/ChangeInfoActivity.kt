package com.example.exercise6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.exercise6.databinding.ActivityChangeInfoBinding
import com.example.exercise6.databinding.ActivityMainBinding

class ChangeInfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChangeInfoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangeInfoBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        init()

    }

    fun init() {
        binding.saveBtn.setOnClickListener {
            if (binding.firsName1.text.isNullOrEmpty() || binding.lastName1.text!!.isNullOrEmpty() || binding.email1.text.isNullOrEmpty() ||
                binding.birthYear1.text.isNullOrEmpty() || binding.gender1.text.isNullOrEmpty()
            )
                Toast.makeText(this, "please fill all fields", Toast.LENGTH_SHORT).show()
            else save()
        }
    }


    private fun save() {
        val firstName = binding.firsName1.text.toString()
        val lastName = binding.lastName1.text.toString()
        val email = binding.email1.text.toString()
        val year = binding.birthYear1.text.toString()
        val gender = binding.gender1.text.toString()
        val changedInfo = UserInfo(firstName, lastName, email, year, gender)
        val intent = intent
        intent.putExtra("result", changedInfo)
        setResult(RESULT_OK, intent)
        finish()


    }
}