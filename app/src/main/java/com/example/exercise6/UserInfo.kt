package com.example.exercise6

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UserInfo(var name: String, var lastName: String, var email: String, var birthYear: String, var gender: String): Parcelable

